package pl.edu.pw.networking;

public class Descriptor {

    private int descrType;
    private String url;

    private String value;

    public Descriptor() {}

    public Descriptor(int descrType, String url) {
        this.descrType = descrType;
        this.url = url;
    }

    public String getValue() {
        return value;
    }

}
