package pl.edu.pw.networking;

public class Distance {

    private int descrType;
    private String url1, url2;

    private double dist;

    public Distance(int descrType, String url1, String url2) {
        this.descrType = descrType;
        this.url1 = url1;
        this.url2 = url2;
    }

    public double getDist() {
        return dist;
    }
}
