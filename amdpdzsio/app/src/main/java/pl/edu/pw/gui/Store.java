package pl.edu.pw.gui;

import android.app.Application;
import android.content.Intent;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import pl.edu.pw.networking.JsonMpegApi;
import pl.edu.pw.networking.Picture;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Store extends Application {

    private static final String TAG = "Store";

    private RecyclerView picRecView;
    protected List<Picture> global_pictures;

    public List<Picture> getGlobal_pictures() {
        return global_pictures;
    }

    public static String serviceUrl = "http://ztv.ire.pw.edu.pl:5984/mpeg7-web/webresources/";
//    public static String serviceUrl = "http://ant.ire.pw.edu.pl:441/mpeg7-web/webresources/";

    private ArrayList<Picture> pictures;

    public void setGlobal_pictures(List<Picture> global_pictures) {
        this.global_pictures = global_pictures;
    }

    public ArrayList<Picture> getPictures() {
        return pictures;
    }

    public Store()
    {
        downloadURLs();
    }

    private void downloadURLs()
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(serviceUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonMpegApi jsonMpegApi = retrofit.create(JsonMpegApi.class);
        Call<List<Picture>> call = jsonMpegApi.getPictures();

        call.enqueue(new Callback<List<Picture>>() {
            @Override
            public void onResponse(Call<List<Picture>> call, Response<List<Picture>> response) {
                if(!response.isSuccessful()){
//                    textViewResult.setText("Code: " + response.code());
                    return;
                }

                global_pictures = response.body();
                initPictureArrayList();

                start();
            }

            @Override
            public void onFailure(Call<List<Picture>> call, Throwable t) {
//                textViewResult.setText(t.getMessage());
            }
        });

    }

    private void initPictureArrayList()
    {
        pictures = new ArrayList<>();

        for (int i = 0; i < global_pictures.size(); i++) {
            String imageUrl = global_pictures.get(i).getUrl();
            int id = global_pictures.get(i).getId();
            pictures.add(new Picture(id, imageUrl));
        }

    }

    private void start()
    {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }



    public String getPictureUrlById(int id)
    {
        for(Picture picture : global_pictures)
        {
            if(picture.getId() == id)
            {
                return picture.getUrl();
            }
        }
        return null;
    }


    public String getPictureUrlByPosition(int position)
    {
        return pictures.get(position).getUrl();
    }


}