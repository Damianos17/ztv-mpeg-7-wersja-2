package pl.edu.pw.gui;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import java.util.ArrayList;
import java.util.List;
import pl.edu.pw.networking.JsonMpegApi;
import pl.edu.pw.networking.Search;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SearchActivity extends AppCompatActivity {

    private static final String TAG = "SearchActivity";
    private RecyclerView picRecView;

    private TextView textViewResult;
    private EditText etUrl, etNumImg;
    private Button btSubmit;
    private ImageView ivPattern, ivChoose;
    private Spinner spinner;
    private int memorizedPosition;

    JsonMpegApi jsonMpegApi;

    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        textViewResult = findViewById(R.id.text_view_result_s);
        textViewResult.setFocusable(true);
        textViewResult.setFocusableInTouchMode(true);

        Retrofit retrofitM = new Retrofit.Builder()
                .baseUrl(Store.serviceUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonMpegApi = retrofitM.create(JsonMpegApi.class);

        init();
    }

    public void init() {
        etUrl = findViewById(R.id.et_url_pattern);
        btSubmit = findViewById(R.id.bt_submit_s);
        ivPattern = findViewById(R.id.iv_pattern);
        spinner = findViewById(R.id.spinner_s);

        ivChoose = findViewById(R.id.iv_choose_s);
        etNumImg = findViewById(R.id.et_num_img);

        initSpinner();
        initButton();
        initEditText();

        initActionBar();
        initIcon();

        initButtons();
        initScrollView();

        initLayout();
        initIntent();
    }

    private void initActionBar() { this.getSupportActionBar().setDisplayHomeAsUpEnabled(true); }

    private void initEditText() {
        etUrl.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                loadImage();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        textViewResult.requestFocusFromTouch();
        textViewResult.requestFocus();

    }

    private void loadImage() {
        Glide.with(this)
                .asBitmap()
                .load(etUrl.getText().toString())
                .into(ivPattern);
    }

    public void initSpinner() {

        String[] descTypes = new String[]{"Dominant Color", "Scalable Color", "Color Layout", "Color Structure", "CT Browsing", "Deskryptor"};
        ArrayAdapter<String> descsAdapter = new ArrayAdapter<String>(this, R.layout.style_spinner_black, descTypes) {
            @Override
            public int getCount() {
                return descTypes.length-1;
            }
        };

        descsAdapter.setDropDownViewResource(R.layout.color_spinner_layout);
        spinner.setAdapter(descsAdapter);
        spinner.setSelection(descTypes.length-1, false);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int yourDescType = (spinner.getSelectedItemPosition() + 1);

                closeKeyboard();
                textViewResult.requestFocusFromTouch();
                textViewResult.requestFocus();

                textViewResult.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                textViewResult.requestLayout();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void initButton() {
        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //linearLayout.getLayoutParams().height = 850; // w xml było 450dp na ll i 300dp dla cv

                try{
                    int yourDescType = (spinner.getSelectedItemPosition() + 1);
                    int numImg = Integer.parseInt(etNumImg.getText().toString());
                    searchImages(yourDescType, numImg, etUrl.getText().toString());
                }catch(Exception e){
                    textViewResult.setText("Błąd wczytywania danych!");
                }

                closeKeyboard();
                textViewResult.requestFocusFromTouch();
                textViewResult.requestFocus();

                textViewResult.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                textViewResult.requestLayout();
            }
        });

    }

    public void searchImages(int descrType, int id, String url) {
        Search search = new Search(descrType, id, url);
        Call<List<Search>> call = jsonMpegApi.searchImages(search);

        call.enqueue(new Callback<List<Search>>() {
            @Override
            public void onResponse(Call<List<Search>> call, Response<List<Search>> response) {
                if (!response.isSuccessful()) {
                    textViewResult.setText("Code: " + response.code()
                            + "Response body: " + response.body()
                            + "Response message" + response.message());
                    return;
                }

                List<Search> searchResponse = response.body();
                textViewResult.setText("Wyniki wyszukiwania:");
                display(searchResponse);

            }

            @Override
            public void onFailure(Call<List<Search>> call, Throwable t) {
                textViewResult.setText(t.getMessage());
            }
        });
    }


    private void closeKeyboard()
    {
        View view = this.getCurrentFocus();
        if(view != null)
        {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    private void initIcon()
    {
        ivChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SearchActivity.this, AllPicturesActivity.class);
                intent.putExtra("position", memorizedPosition);
                intent.putExtra("from", "SearchActivity");
                startActivityForResult(intent, 1);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 1)
        {
            if(resultCode == RESULT_OK)
            {
                memorizedPosition = data.getIntExtra("position", 0);
                String temp_url = data.getStringExtra("pictureUrl");
                etUrl.setText(temp_url);
            }
        }
    }

    private void initButtons()
    {
    }

    private void initScrollView()
    {

    }


    @SuppressLint("WrongConstant")
    private void display(List<Search> searchResponse)
    {
        Log.d(TAG, "onCreate: started");

        picRecView = (RecyclerView) findViewById(R.id.picRecView_s);


//        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(this);
//        layoutManager.setFlexDirection(FlexDirection.COLUMN);
//        layoutManager.setJustifyContent(JustifyContent.SPACE_AROUND);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayout.HORIZONTAL);

        picRecView.setLayoutManager(layoutManager);

        SearchRecViewAdapter adapter = new SearchRecViewAdapter(this);
        picRecView.setAdapter(adapter);

        ArrayList<Search> searches = new ArrayList<>();

        for (int i = 0; i < searchResponse.size(); i++)
        {
            String imageUrl = searchResponse.get(i).getUrl();
            int id = searchResponse.get(i).getId();
            double distance = searchResponse.get(i).getDist();
            searches.add(new Search(id, imageUrl, distance));
        }
        adapter.setPictures(searches);
    }

    private void initLayout()
    {
        linearLayout = findViewById(R.id.linear_layout);
    }

    private void initIntent()
    {
        Intent intent = getIntent();
        memorizedPosition = intent.getIntExtra("position", 0);
        String temp_url = intent.getStringExtra("pictureUrl");
        etUrl.setText(temp_url);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

