package pl.edu.pw.gui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

public class PictureActivity extends AppCompatActivity {

    private TextView pictureUrl;
    private ImageView pictureImage;

    private Button btnPrev, btnNext, btnSearch;

    private int position, size;
    private String temp_url;

    Spinner spinner;
    boolean initDisplay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture);

        initWidgets();

        Intent intent = getIntent();
        position = intent.getIntExtra("position", 0);
        temp_url = intent.getStringExtra("pictureUrl");

        Glide.with(this)
                .asBitmap()
                .load(temp_url)
                .into(pictureImage);

        pictureImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showUrl();
                copyUrl();
            }
        });
        refresh();

        initSpinner();
    }

    private void initWidgets() {
        pictureUrl = (TextView) findViewById(R.id.pictureUrl);
        pictureImage = (ImageView) findViewById(R.id.pictureImage);

        //getSupportActionBar().hide();
        initButtons();
        initActionBar();
    }

    private void initActionBar() { this.getSupportActionBar().setDisplayHomeAsUpEnabled(true); }

    private void copyUrl()
    {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("url obrazka", pictureUrl.getText().toString());
        clipboard.setPrimaryClip(clip);

        //Toast.makeText(this, "skopiowano: " + clip.getDescription(), Toast.LENGTH_SHORT).show();
    }

    private void showUrl()
    {
        Toast toast= Toast.makeText(this, temp_url, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP| Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void initButtons()
    {
        size = ((Store) this.getApplication()).global_pictures.size();

        btnPrev = (Button) findViewById(R.id.buttonPrevious);
        btnNext = (Button) findViewById(R.id.buttonNext);
        btnSearch = (Button) findViewById(R.id.button_search);

        btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                position -= 1;
                refresh();
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                position += 1;
                refresh();
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PictureActivity.this, SearchActivity.class);
                intent.putExtra("pictureUrl", temp_url);
                intent.putExtra("position", position);
                startActivity(intent);
            }
        });
    }

    private void refresh()
    {
        if(position < 0)
            position = size - 1;
        if(position >= size)
            position = 0;

        temp_url = ((Store) this.getApplication()).getPictureUrlByPosition(position);

        Glide.with(this)
                .asBitmap()
                .load(temp_url)
                .into(pictureImage);

        pictureUrl.setText("pozycja = " + position + ". A global size = "+size);

    }

    private void initSpinner()
    {
        initDisplay = false;
        spinner = findViewById(R.id.spinner);
        String[] descTypes = new String[]{"Dominant Color", "Scalable Color", "Color Layout", "Color Structure", "CT Browsing", "Deskryptor"};
        ArrayAdapter<String> descsAdapter = new ArrayAdapter<String>(this, R.layout.style_spinner, descTypes) {
            @Override
            public int getCount() {
                return descTypes.length-1;
            }
        };

        descsAdapter.setDropDownViewResource(R.layout.color_spinner_layout);
        spinner.setAdapter(descsAdapter);
        spinner.setSelection(descTypes.length-1, false);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                if(initDisplay == true && view != null)
                {
                    int yourDescType = (spinner.getSelectedItemPosition() + 1);
                    Intent intent = new Intent(PictureActivity.this, DescriptorActivity.class);
                    intent.putExtra("pictureUrl", temp_url);
                    intent.putExtra("position", position);
                    intent.putExtra("descrType", yourDescType);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        initDisplay = true;
    }

}
