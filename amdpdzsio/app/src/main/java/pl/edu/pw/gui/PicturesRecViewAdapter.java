package pl.edu.pw.gui;


import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import pl.edu.pw.networking.Picture;

public class PicturesRecViewAdapter extends RecyclerView.Adapter<PicturesRecViewAdapter.ViewHolder> {
    private static final String TAG = "PicturesRecViewAdapter";

    private ArrayList<Picture> pictures = new ArrayList<>();
    private Context context;

    public PicturesRecViewAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_picture_rec_view, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.pictureUrl.setText(pictures.get(position).getUrl());

        holder.pictureCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PictureActivity.class);
                intent.putExtra("pictureUrl", pictures.get(position).getUrl());
                intent.putExtra("position", position);
                context.startActivity(intent);

            }
        });

        Glide.with(context)
                .asBitmap()
                .load(pictures.get(position).getUrl())
                .into(holder.pictureImage);
    }

    @Override
    public int getItemCount() {
        return pictures.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView pictureImage;
        private TextView pictureUrl;
        private CardView pictureCard;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            pictureImage = (ImageView) itemView.findViewById(R.id.pictureImage);
            pictureUrl = (TextView) itemView.findViewById(R.id.pictureUrl);
            pictureCard = (CardView) itemView.findViewById(R.id.pictureCard);
        }
    }

    public void setPictures(ArrayList<Picture> pictures) {
        this.pictures = pictures;
        notifyDataSetChanged();
    }

}



