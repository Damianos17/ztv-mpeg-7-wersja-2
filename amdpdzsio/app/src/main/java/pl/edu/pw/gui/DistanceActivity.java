package pl.edu.pw.gui;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import pl.edu.pw.networking.Distance;
import pl.edu.pw.networking.JsonMpegApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DistanceActivity extends AppCompatActivity {

    private TextView textViewResult;
    private EditText etUrl1, etUrl2;
    private Button btSubmit, btnImage1, btnImage2;
    private ImageView ivResult1, ivResult2, ivChoose1, ivChoose2;
    private Spinner spinner;
    private int memorizedPosition1, memorizedPosition2;

    JsonMpegApi jsonMpegApi;

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distance);

        textViewResult = findViewById(R.id.text_view_result);

        Retrofit retrofitM = new Retrofit.Builder()
                .baseUrl(Store.serviceUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonMpegApi = retrofitM.create(JsonMpegApi.class);

        init();
    }

    public void init()
    {
        etUrl1 = findViewById(R.id.et_url);
        etUrl2 = findViewById(R.id.et_url2);
        btSubmit = findViewById(R.id.bt_submit);
        ivResult1 = findViewById(R.id.iv_result);
        ivResult2 = findViewById(R.id.iv_result2);
        spinner = findViewById(R.id.spinner);

        btnImage1 = findViewById(R.id.btn_image1);
        btnImage2 = findViewById(R.id.btn_image2);

        ivChoose1 = findViewById(R.id.iv_choose1);
        ivChoose2 = findViewById(R.id.iv_choose2);

        initSpinner();
        initButtons();

        initActionBar();
        initEditTexts();
        initIcons();
    }

    private void initActionBar() { this.getSupportActionBar().setDisplayHomeAsUpEnabled(true); }

    public void initSpinner()
    {
        String[] descTypes = new String[]{"Dominant Color", "Scalable Color", "Color Layout", "Color Structure", "CT Browsing", "Deskryptor"};
        ArrayAdapter<String> descsAdapter = new ArrayAdapter<String>(this, R.layout.style_spinner_black, descTypes) {
            @Override
            public int getCount() {
                return descTypes.length-1;
            }
        };

        descsAdapter.setDropDownViewResource(R.layout.color_spinner_layout);
        spinner.setAdapter(descsAdapter);
        spinner.setSelection(descTypes.length-1);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int yourDescType = (spinner.getSelectedItemPosition() + 1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public void initButtons()
    {
        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int yourDescType = (spinner.getSelectedItemPosition() + 1);
                calculateDistance(yourDescType, etUrl1.getText().toString(), etUrl2.getText().toString());
            }
        });

        btnImage1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DistanceActivity.this, AllPicturesActivity.class);
                intent.putExtra("position", memorizedPosition1);
                intent.putExtra("from", "DistanceActivityImg1");
                startActivityForResult(intent, 1);
            }
        });

        btnImage2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DistanceActivity.this, AllPicturesActivity.class);
                intent.putExtra("position", memorizedPosition2);
                intent.putExtra("from", "DistanceActivityImg2");
                startActivityForResult(intent, 1);
            }
        });
    }

    public void calculateDistance(int descrType, String url1, String url2)
    {
        Distance distance = new Distance(descrType, url1, url2);
        Call<Distance> call = jsonMpegApi.calculateDistance(distance);

        call.enqueue(new Callback<Distance>() {
            @Override
            public void onResponse(Call<Distance> call, Response<Distance> response) {
                if(!response.isSuccessful()){
                    textViewResult.setText("Błąd: " + response.message());
                    return;
                }

                Distance distResponse = response.body();

                String formattedString = String.format("%.04f", distResponse.getDist());

                String content = "";
                content += "Odległość: " + formattedString + "\n";

                textViewResult.setText(content);
            }

            @Override
            public void onFailure(Call<Distance> call, Throwable t) {
                textViewResult.setText(t.getMessage());
            }
        });

    }

    private void loadImage1() {
        Glide.with(this)
                .asBitmap()
                .load(etUrl1.getText().toString())
                .into(ivResult1);
    }

    private void loadImage2() {
        Glide.with(this)
                .asBitmap()
                .load(etUrl2.getText().toString())
                .into(ivResult2);
    }


    private void initEditTexts() {
        etUrl1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                loadImage1();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etUrl2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                loadImage2();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1)
        {
            if(resultCode == RESULT_OK)
            {
                int imgNumber = data.getIntExtra("img", 1);
                String temp_url = data.getStringExtra("pictureUrl");

                if(imgNumber == 1)
                {
                    memorizedPosition1 = data.getIntExtra("position", 0);
                    etUrl1.setText(temp_url);
                }
                else if(imgNumber == 2)
                {
                    memorizedPosition2 = data.getIntExtra("position", 0);
                    etUrl2.setText(temp_url);
                }
            }
        }
    }


    private void initIcons()
    {
        ivChoose1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DistanceActivity.this, AllPicturesActivity.class);
                intent.putExtra("position", memorizedPosition1);
                intent.putExtra("from", "DistanceActivityImg1");
                startActivityForResult(intent, 1);
            }
        });

        ivChoose2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DistanceActivity.this, AllPicturesActivity.class);
                intent.putExtra("position", memorizedPosition2);
                intent.putExtra("from", "DistanceActivityImg2");
                startActivityForResult(intent, 1);
            }
        });
    }




}




























