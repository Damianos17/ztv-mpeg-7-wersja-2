package pl.edu.pw.gui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

public class ChoosingActivity extends AppCompatActivity {

    private TextView pictureUrl;
    private ImageView pictureImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choosing);

        initWidgets();

        Intent intent = getIntent();
        String temp_url = intent.getStringExtra("pictureUrl");

        pictureUrl.setText(temp_url);

        Glide.with(this)
                .asBitmap()
                .load(temp_url)
                .into(pictureImage);

        pictureImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                copyUrl();
            }
        });
    }


    private void initWidgets()
    {
        pictureUrl = (TextView) findViewById(R.id.pictureUrl);
        pictureImage = (ImageView) findViewById(R.id.pictureImage);
    }

    private void copyUrl()
    {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("url obrazka", pictureUrl.getText().toString());
        clipboard.setPrimaryClip(clip);

        String clipboardText = clipboard.getPrimaryClip().getItemAt(0)
                .coerceToText(getApplicationContext()).toString();

        Toast.makeText(this, "skopiowano: " + clipboardText, Toast.LENGTH_SHORT).show();

    }
}