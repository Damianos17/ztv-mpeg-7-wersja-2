package pl.edu.pw.gui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import pl.edu.pw.networking.Search;

public class SearchRecViewAdapter extends RecyclerView.Adapter<SearchRecViewAdapter.ViewHolder> {
    private static final String TAG = "SearchRecViewAdapter";

    private ArrayList<Search> foundPictures = new ArrayList<>();
    private Context context;
    private TextView textView;

    public SearchRecViewAdapter(Context context) {
        this.context = context;
    }

    public SearchRecViewAdapter(Context context, TextView textView) {
        this.context = context;
        this.textView = textView;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_search_rec_view, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.pictureUrl.setText(foundPictures.get(position).getUrl());

        holder.pictureCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ewentualnie odległość wyświetlać po kliknięciu
//                Intent intent = new Intent(context, MainActivity.class);
//                intent.putExtra("pictureUrl", foundPictures.get(position).getUrl());
//                intent.putExtra("position", position);
//                context.startActivity(intent);

                textView.setVisibility(View.VISIBLE);
                textView.setText("Odległość: " + foundPictures.get(position).getDist());

            }
        });

        Glide.with(context)
                .asBitmap()
                .load(foundPictures.get(position).getUrl())
                .into(holder.pictureImage);
    }

    @Override
    public int getItemCount() {
        return foundPictures.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView pictureImage;
        private TextView pictureUrl;
        private CardView pictureCard;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            pictureImage = (ImageView) itemView.findViewById(R.id.pictureImage_s);
            pictureUrl = (TextView) itemView.findViewById(R.id.pictureUrl_s);
            pictureCard = (CardView) itemView.findViewById(R.id.pictureCard_s);
        }
    }

    public void setPictures(ArrayList<Search> foundPictures) {
        this.foundPictures = foundPictures;
        notifyDataSetChanged();
    }



}
