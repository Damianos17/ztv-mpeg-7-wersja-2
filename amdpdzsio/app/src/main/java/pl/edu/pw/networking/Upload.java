package pl.edu.pw.networking;

public class Upload
{
    private int descrType, numImg;

    private int id;
    private String url;
    private double dist;

    private String data, filename;

    public Upload(int descrType, int numImg, String data, String filename) {
        this.filename = filename;
        this.descrType = descrType;
        this.numImg = numImg;
        this.data = data;
    }

    public Upload(int id, String url, double dist) {
        this.id = id;
        this.url = url;
        this.dist = dist;
    }

    public int getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public double getDist() {
        return dist;
    }


}
