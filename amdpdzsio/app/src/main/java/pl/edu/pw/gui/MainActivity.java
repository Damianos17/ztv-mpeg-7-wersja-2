package pl.edu.pw.gui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout dl;
    private ActionBarDrawerToggle abdt;
    private TextView txtMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initWidgets();
        initNavigationBar();
    }

    private void initWidgets(){
        //this.getSupportActionBar().hide();

        txtMain = (TextView) findViewById(R.id.text_view_main);
        txtMain.setText("" +
                "\n" +
                "Standard MPEG-7 definiuje szereg deskryptorów treści multimedialnych, zarówno wysokopoziomowych - związanych z semantyką danych, jak i niskopoziomowych - powiązanych z fizycznymi parametrami sygnału danych, ułatwiających realizację wyszukiwania, przeglądania i filtrowania danych multimedialnych. Za pomocą cech wysokopoziomowych można opisać cechy niedostępne w sposób bezpośredni w treści np. autor, data produkcji, warunki rejestracji, gatunek, prawa autorskie itp., a także dokonać opisu związanego z ludzką interpretacją treści materiału audiowizualnego. Cechy niskopoziomowe nie posiadają wartości semantycznej, ale mogą być w prosty sposób wyliczane i porównywane.\n" +
                "\n" +
                "W przypadku danych wizualnych, deskryptory MPEG-7 pozwalają na opis takich aspektów danych jak kolor, tekstura, kształt i ruch. Niektóre z tych cech mogą dotyczyć całości np. kolorystyka obrazu, intensywność ruchu w sekwencji wideo, inne powiązane są z poszczególnymi obiektami występującymi w scenie np. kolor, tekstura i kształt obiektu, trajektoria ruchu, lokalizacja obiektu w czasie i przestrzeni, dlatego też w celu ich wyliczenia konieczne jest odpowiednie przygotowanie danych (np. segmentacja obiektów).\n\n\n");

    }

    private void initNavigationBar()
    {
        dl = (DrawerLayout) findViewById(R.id.dl);
        abdt = new ActionBarDrawerToggle(this, dl, R.string.Open, R.string.Close);
        abdt.setDrawerIndicatorEnabled(true);

        dl.addDrawerListener(abdt);
        abdt.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int id = menuItem.getItemId();

                if(id == R.id.m_picture_database)
                {
                    Intent intent = new Intent(MainActivity.this, PictureDbActivity.class);
                    startActivity(intent);
                }
                else if(id == R.id.m_descriptors)
                {
                    Intent intent = new Intent(MainActivity.this, DescriptorActivity.class);
                    startActivity(intent);
                }
                else if(id == R.id.m_distance)
                {
                    Intent intent = new Intent(MainActivity.this, DistanceActivity.class);
                    startActivity(intent);
                }
                else if(id == R.id.m_search)
                {
                    Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                    startActivity(intent);
                }
                else if(id == R.id.m_upload)
                {
                    Intent intent = new Intent(MainActivity.this, UploadActivity.class);
                    startActivity(intent);
                }

                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return abdt.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }
}




















