package pl.edu.pw.gui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;

public class PictureDbActivity extends AppCompatActivity implements DbDialog.DbDialogListener {

    private static final String TAG = "PictureDbActivity";

    private RecyclerView picRecView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture_db);

        init();
        display();
    }

    private void init()
    {
        initActionBar();
    }

    private void initActionBar() { this.getSupportActionBar().setDisplayHomeAsUpEnabled(true); }

    private void display()
    {
        setContentView(R.layout.activity_picture_db);
        Log.d(TAG, "onCreate: started");

        picRecView = (RecyclerView) findViewById(R.id.picRecView);

        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(this);
        layoutManager.setFlexDirection(FlexDirection.ROW);
        layoutManager.setJustifyContent(JustifyContent.SPACE_AROUND);
        picRecView.setLayoutManager(layoutManager);

        PicturesRecViewAdapter adapter = new PicturesRecViewAdapter(this);
        picRecView.setAdapter(adapter);

        adapter.setPictures(((Store) this.getApplication()).getPictures());
    }

    /**
     * CTRL+O
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.picture_db_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId())
        {
            case R.id.m_scroll:
                //Toast.makeText(this, "Item 1 selected", Toast.LENGTH_SHORT).show();
                openDialog();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openDialog()
    {
        DbDialog dbDialog = new DbDialog();
        dbDialog.show(getSupportFragmentManager(), "example dialog");
    }

    @Override
    public void applyTexts(String positionString)
    {
        int positionNumber = Integer.parseInt(positionString);
        picRecView.scrollToPosition(positionNumber);

        Toast.makeText(this, "Pozycja: " + positionString, Toast.LENGTH_SHORT).show();
    }
}
