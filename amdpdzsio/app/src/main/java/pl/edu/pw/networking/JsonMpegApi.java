package pl.edu.pw.networking;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface JsonMpegApi {

    @GET("image")
    Call<List<Picture>> getPictures();

    @FormUrlEncoded
    @POST("calcdescr")
    Call<Descriptor> createPost(
            @Field("descrType") int descrType,
            @Field("url") String url
    );


    @POST("calcdescr")
//    FooResponse postJson(@Body FooRequest body);
    Call<Descriptor> calculateDescriptor(@Body Descriptor descriptor);

    @POST("calc-dist-url")
    Call<Distance> calculateDistance(@Body Distance distance);

    @POST("imgsearch")
    Call<List<Search>> searchImages(@Body Search search);

    @POST("image-upload-search")
    Call<List<Upload>> uploadImage(@Body Upload upload);
}