package pl.edu.pw.gui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;

public class AllPicturesActivity extends AppCompatActivity implements DbDialog.DbDialogListener {

    private static final String TAG = "AllPicturesActivity";

    private RecyclerView picRecView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_pictures);

        display();
        initActionBar();
    }

    private void display()
    {
        setContentView(R.layout.activity_all_pictures);
        Log.d(TAG, "onCreate: started");

        picRecView = (RecyclerView) findViewById(R.id.picRecView);

        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(this);
        layoutManager.setFlexDirection(FlexDirection.ROW);
        layoutManager.setJustifyContent(JustifyContent.SPACE_AROUND);
        picRecView.setLayoutManager(layoutManager);

        Intent intent = getIntent();
        int position = intent.getIntExtra("position", 0);
        String from = intent.getStringExtra("from");

        ChoosingRecViewAdapter adapter = new ChoosingRecViewAdapter(this, this, from);
        picRecView.setAdapter(adapter);

        adapter.setPictures(((Store) this.getApplication()).getPictures());

        picRecView.scrollToPosition(position);

    }

    private void initActionBar() { this.getSupportActionBar().setDisplayHomeAsUpEnabled(true); }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch(item.getItemId())
        {
            case R.id.m_scroll:
                //Toast.makeText(this, "Item 1 selected", Toast.LENGTH_SHORT).show();
                openDialog();
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.picture_db_menu, menu);
        return true;
    }

    private void openDialog()
    {
        DbDialog dbDialog = new DbDialog();
        dbDialog.show(getSupportFragmentManager(), "example dialog");
    }

    @Override
    public void applyTexts(String positionString)
    {
        int positionNumber = Integer.parseInt(positionString);
        picRecView.scrollToPosition(positionNumber);
    }

}
