package pl.edu.pw.networking;

public class Search
{
    private int descrType, numImg;

    private int id;
    private String url;
    private double dist;

    public Search(int descrType, int numImg, String url) {
        this.descrType = descrType;
        this.numImg = numImg;
        this.url = url;
    }

    public Search(int id, String url, double dist) {
        this.id = id;
        this.url = url;
        this.dist = dist;
    }

    public int getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public double getDist() {
        return dist;
    }
}
