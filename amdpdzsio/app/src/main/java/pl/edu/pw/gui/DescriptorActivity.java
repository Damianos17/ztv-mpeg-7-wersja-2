package pl.edu.pw.gui;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import pl.edu.pw.networking.Descriptor;
import pl.edu.pw.networking.JsonMpegApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DescriptorActivity extends AppCompatActivity {

    private TextView textViewResult;
    private EditText etUrl1;
    private Button btSubmit, btZoomIn, btZoomOut, btCopy;
    private ImageView ivResult1, ivChoose;
    private Spinner spinner;
    private int memorizedPosition;

    JsonMpegApi jsonMpegApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_descriptor);

        textViewResult = findViewById(R.id.text_view_result);
        textViewResult.setHorizontallyScrolling(true);
        textViewResult.setMovementMethod(new ScrollingMovementMethod());
        textViewResult.setFocusable(true);
        textViewResult.setFocusableInTouchMode(true);

        Retrofit retrofitM = new Retrofit.Builder()
                .baseUrl(Store.serviceUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonMpegApi = retrofitM.create(JsonMpegApi.class);

        init();
    }

    public void init() {
        etUrl1 = findViewById(R.id.et_url);
        btSubmit = findViewById(R.id.bt_submit);
        ivResult1 = findViewById(R.id.iv_result);
        spinner = findViewById(R.id.spinner);

        ivChoose = findViewById(R.id.iv_choose);

        initSpinner();
        initButton();
        initEditText();

        initActionBar();
        initIcon();

        initButtons();

        initIntent();
    }

    private void initActionBar() { this.getSupportActionBar().setDisplayHomeAsUpEnabled(true); }

    private void initEditText() {
        etUrl1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                loadImage();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        textViewResult.requestFocusFromTouch();
        textViewResult.requestFocus();

    }

    private void loadImage() {
        Glide.with(this)
                .asBitmap()
                .load(etUrl1.getText().toString())
                .into(ivResult1);
    }

    public void initSpinner() {

        String[] descTypes = new String[]{"Dominant Color", "Scalable Color", "Color Layout", "Color Structure", "CT Browsing", "Deskryptor"};
        ArrayAdapter<String> descsAdapter = new ArrayAdapter<String>(this, R.layout.style_spinner_black, descTypes) {
            @Override
            public int getCount() {
                return descTypes.length-1;
            }
        };

        descsAdapter.setDropDownViewResource(R.layout.color_spinner_layout);
        spinner.setAdapter(descsAdapter);
        spinner.setSelection(descTypes.length-1, false);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int yourDescType = (spinner.getSelectedItemPosition() + 1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    public void initButton() {
        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int yourDescType = (spinner.getSelectedItemPosition() + 1);
                try{
                    calculateDescriptor(yourDescType, etUrl1.getText().toString());
                }catch(Exception e){
                    etUrl1.setText("Błąd wczytywania danych!");
                }

                closeKeyboard();
                textViewResult.requestFocusFromTouch();
                textViewResult.requestFocus();

                textViewResult.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                textViewResult.requestLayout();
            }
        });

    }

    public void calculateDescriptor(int descrType, String url) {
        Descriptor descriptor = new Descriptor(descrType, url);
        Call<Descriptor> call = jsonMpegApi.calculateDescriptor(descriptor);

        call.enqueue(new Callback<Descriptor>() {
            @Override
            public void onResponse(Call<Descriptor> call, Response<Descriptor> response) {
                if (!response.isSuccessful()) {
                    textViewResult.setText("Code: " + response.code()
                            + "Response body: " + response.body()
                            + "Response message" + response.message());
                    return;
                }
                Descriptor descResponse = response.body();

                String content = "";
                content += "Wynik:\n" + descResponse.getValue() + "";
                textViewResult.setText(content);
            }

            @Override
            public void onFailure(Call<Descriptor> call, Throwable t) {
                textViewResult.setText(t.getMessage());
            }
        });
    }


    private void closeKeyboard()
    {
        View view = this.getCurrentFocus();
        if(view != null)
        {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    private void initIcon()
    {
        ivChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DescriptorActivity.this, AllPicturesActivity.class);
                intent.putExtra("position", memorizedPosition);
                intent.putExtra("from", "DescriptorActivity");
                startActivityForResult(intent, 1);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1)
        {
            if(resultCode == RESULT_OK)
            {
                memorizedPosition = data.getIntExtra("position", 0);
                String temp_url = data.getStringExtra("pictureUrl");
                etUrl1.setText(temp_url);
            }
        }
    }

    private void initButtons()
    {
        btZoomIn = findViewById(R.id.btn_zoom_in);
        btZoomOut = findViewById(R.id.btn_zoom_out);
        btCopy = findViewById(R.id.btn_copy);

        btZoomOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int height = textViewResult.getHeight();
                float px = textViewResult.getTextSize();
                float sp = px / getResources().getDisplayMetrics().scaledDensity;
                float newTextSize = sp - 1;
                textViewResult.setTextSize(TypedValue.COMPLEX_UNIT_SP, newTextSize);
                textViewResult.getLayoutParams().height = height;
                textViewResult.requestLayout();
            }
        });

        btZoomIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int height = textViewResult.getHeight();
                float px = textViewResult.getTextSize();
                float sp = px / getResources().getDisplayMetrics().scaledDensity;
                float newTextSize = sp + 1;
                textViewResult.setTextSize(TypedValue.COMPLEX_UNIT_SP, newTextSize);
                textViewResult.getLayoutParams().height = height;
                textViewResult.requestLayout();
            }
        });

        btCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                copyValue();
            }
        });
    }



    private void copyValue()
    {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("descriptor value", textViewResult.getText().toString());
        clipboard.setPrimaryClip(clip);

        String clipboardText = clipboard.getPrimaryClip().getItemAt(0)
                .coerceToText(getApplicationContext()).toString();

        Toast.makeText(this, "Zawartość skopiowana: " + clipboardText, Toast.LENGTH_SHORT).show();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initIntent()
    {
        Intent intent = getIntent();
        int flag = intent.getFlags();

        if(flag == Intent.FLAG_ACTIVITY_NEW_TASK)
        {
            memorizedPosition = intent.getIntExtra("position", 0);
            String temp_url = intent.getStringExtra("pictureUrl");
            int descrType = intent.getIntExtra("descrType", 0);
            spinner.setSelection(descrType-1);
            etUrl1.setText(temp_url);
            btSubmit.callOnClick();
        }

    }

}

