package pl.edu.pw.gui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import pl.edu.pw.networking.Picture;

import static android.app.Activity.RESULT_OK;

public class ChoosingRecViewAdapter extends RecyclerView.Adapter<ChoosingRecViewAdapter.ViewHolder> {
    private static final String TAG = "ChoosingRecViewAdapter";

    private ArrayList<Picture> pictures = new ArrayList<>();
    private Context context;

    private String from;
    private Activity activity;

    public ChoosingRecViewAdapter(Context context) {
        this.context = context;
    }

    public ChoosingRecViewAdapter(Activity activity, Context context, String from) {
        this.activity = activity;
        this.context = context;
        this.from = from;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_picture_rec_view, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.pictureUrl.setText(pictures.get(position).getUrl());

        holder.pictureCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent resultIntent;
                if(from.equals("DescriptorActivity") || from.equals("SearchActivity"))
                {
                    resultIntent = new Intent();
                }
                else if(from.equals("DistanceActivityImg1"))
                {
                    resultIntent = new Intent();
                    resultIntent.putExtra("img", 1);
                }
                else if(from.equals("DistanceActivityImg2"))
                {
                    resultIntent = new Intent();
                    resultIntent.putExtra("img", 2);
                }
                else
                {
                    resultIntent = new Intent();
                }

                resultIntent.putExtra("position", position);
                resultIntent.putExtra("pictureUrl", pictures.get(position).getUrl());
                activity.setResult(RESULT_OK, resultIntent);
                activity.finish();
            }
        });

        Glide.with(context)
                .asBitmap()
                .load(pictures.get(position).getUrl())
                .into(holder.pictureImage);
    }

    @Override
    public int getItemCount() {
        return pictures.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private ImageView pictureImage;
        private TextView pictureUrl;
        private CardView pictureCard;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            pictureImage = (ImageView) itemView.findViewById(R.id.pictureImage);
            pictureUrl = (TextView) itemView.findViewById(R.id.pictureUrl);
            pictureCard = (CardView) itemView.findViewById(R.id.pictureCard);
        }
    }

    public void setPictures(ArrayList<Picture> pictures) {
        this.pictures = pictures;
        notifyDataSetChanged();
    }


}