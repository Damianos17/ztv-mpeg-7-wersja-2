package pl.edu.pw.gui;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.BitmapCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.squareup.picasso.Picasso;

import android.graphics.Bitmap;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import pl.edu.pw.networking.JsonMpegApi;
import pl.edu.pw.networking.Search;
import pl.edu.pw.networking.Upload;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class UploadActivity extends AppCompatActivity {

    private static final String TAG = "UploadActivity";

    private static final int PICK_IMAGE_REQUEST = 1;

    private Button mButtonChooseImage, mButtonUpload;
    private TextView mTextViewShowUploads, mTextViewUri, mTextViewMain, mTextViewSize;
    private EditText mEditTextFileName;
    private ImageView mImageView;
    //private ProgressBar mProgressBar;

    private Uri mImageUri;
    JsonMpegApi jsonMpegApi;

    String bitmapS;
    RadioGroup radioGroup;
    RadioButton radioButton;
    int quality;

    private RecyclerView picRecView;
    private Spinner spinner;
    private EditText etNumImg;

    private Double photoSize;
    //Bitmap bitmap;

    long compressionStartTime, compressionEndTime, uploadStartTime, uploadEndTime;
    private ScrollView scrollView;

    SearchRecViewAdapter adapter;

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);

        mButtonChooseImage = findViewById(R.id.button_choose_image);
        mButtonUpload = findViewById(R.id.button_upload);
//        mEditTextFileName = findViewById(R.id.edit_text_file_name);
        mImageView = findViewById(R.id.image_view);

        mTextViewUri = findViewById(R.id.uri_text_view);
        mTextViewMain = findViewById(R.id.text_view_main);
        mTextViewSize = findViewById(R.id.text_view_size);

        Retrofit retrofitM = new Retrofit.Builder()
                .baseUrl(Store.serviceUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonMpegApi = retrofitM.create(JsonMpegApi.class);


        mButtonChooseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFileChooser();
            }
        });

       mButtonUpload.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
                mButtonUpload.setVisibility(View.INVISIBLE);
//                getWindow().getDecorView().findViewById(android.R.id.content).invalidate();
//                recreate();
                upload();
           }
       });

       radioGroup = findViewById(R.id.radio_group);
       quality = 100;
       spinner = findViewById(R.id.spinner_s);
       etNumImg = findViewById(R.id.et_num_img);

        /***
         *
         * adapter przeniesienie
         */

        picRecView = (RecyclerView) findViewById(R.id.picRecView_u);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayout.HORIZONTAL);
        picRecView.setLayoutManager(layoutManager);
        adapter = new SearchRecViewAdapter(this, mTextViewUri);
        picRecView.setAdapter(adapter);

        //

        init();


    }

    /**
     * Pomiary
     */
    private void upload()
    {
        //Toast.makeText(this, "quality: " + quality, Toast.LENGTH_SHORT).show();
        String measurement = "";

        compressionStartTime = System.currentTimeMillis();
        compressNHold();
        //compressAutomatically();
        compressionEndTime = System.currentTimeMillis();

        try
        {


//            int yourDescType = 1;
            int yourDescType = (spinner.getSelectedItemPosition() + 1);
//            int numImg = 10;
            int numImg = Integer.parseInt(etNumImg.getText().toString());


            uploadImage(yourDescType, numImg, bitmapS);


        }
        catch(Exception e)
        {
            mTextViewUri.setText("Błąd wczytywania danych!");
        }
    }

    private void displayTimeMeasurements()
    {
        uploadEndTime = System.currentTimeMillis();
        double d_difference = (double) (compressionEndTime-compressionStartTime)/1000;
        double d_differenceU = (double) (uploadEndTime-uploadStartTime)/1000;
        double operationDuration = (double) (uploadEndTime-compressionStartTime)/1000;

        mTextViewMain.setText("Czas kompresji: " + d_difference + "s");
        mTextViewMain.append("\nCzas odpowiedzi serwera: " + d_differenceU + "s");
        mTextViewMain.append("\nCzas całkowity operacji: " + (operationDuration) + "s");
    }

    private void openFileChooser(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            mImageUri = data.getData();

            //mTextViewUri.setText(mImageUri.toString());

//            Picasso.with(this).load(mImageUri).into(mImageView);
            //Glide.with(this).load(mImageUri).into(mImageView);

            Glide.with(this)
                    .load(mImageUri)
                    .placeholder(android.R.drawable.progress_indeterminate_horizontal)
                    .error(android.R.drawable.stat_notify_error)
                    .into(mImageView);

            Glide.with(this)
                    .asBitmap().load(mImageUri)
                    .listener(new RequestListener<Bitmap>() {
                                  @Override
                                  public boolean onLoadFailed(@Nullable GlideException e, Object o, Target<Bitmap> target, boolean b) {
                                      //Toast.makeText(cxt,getResources().getString(R.string.unexpected_error_occurred_try_again),Toast.LENGTH_SHORT).show();
                                      return false;
                                  }

                                  @Override
                                  public boolean onResourceReady(Bitmap bitmap, Object o, Target<Bitmap> target, DataSource dataSource, boolean b) {
                                      //zoomImage.setImage(ImageSource.bitmap(bitmap));
                                      setPhotoSize(bitmap);
                                      return false;
                                  }
                              }
                    ).submit();


//            Bitmap bitmap = null;
//            try {
//                bitmap = Glide
//                        .with(this)
//                        .asBitmap()
//                        .load(mImageUri);
//
//                setPhotoSize(bitmap);
//
//            } catch (ExecutionException e) {
//                e.printStackTrace();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }



//            try{
//
//                //Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), mImageUri);
//
//
//                setPhotoSize(bitmap);
//            } catch (IOException e) { e.printStackTrace(); }

            mTextViewMain.setVisibility(View.VISIBLE);
            //mTextViewUri.setVisibility(View.VISIBLE);
            mButtonUpload.setVisibility(View.VISIBLE);
            //mImageView.setImageURI(mImageUri);


//            Uri imageUri = data.getData();

        }
    }
//
    private void compressNHold()
    {
            //mImageView.setImageBitmap(bitmap);
            //String bitmapS = BitMapToString(bitmap);
            //bitmapS = convert(bitmap);
            bitmapS = convert();


            //mTextViewUri.setText(bitmapS);

//                mTextViewMain.setText(bitmapS);
    }



    //public String convert(Bitmap _bitmap)
    public String convert()
    {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        //
        Bitmap bitmap = null;
        try {
            bitmap = Glide
                    .with(this)
                    .asBitmap()
                    .load(mImageUri)
                    .submit()
                    .get();

            setPhotoSize(bitmap);

            if(quality == -1)
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
            else
                bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);

        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        /**
         *
         */


//        try {
//            Bitmap tempBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), mImageUri);
//
//            if(quality == -1)
//                tempBitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
////            else if(quality == -1)
////                tempBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
//            else
//                tempBitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
//
//        } catch (IOException e) { e.printStackTrace(); }

        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
    }

    private ByteArrayOutputStream compress(Bitmap _bitmap, int _quality)
    {
        int osSize = -10;
        ByteArrayOutputStream _outputStream = new ByteArrayOutputStream();

//        try {
            //Bitmap tempBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), mImageUri);// wczytanie wybranego z galerii obrazu jako bitmapy
            if(_quality == -1)
                _bitmap.compress(Bitmap.CompressFormat.PNG, 100, _outputStream);
                //tempBitmap.compress(Bitmap.CompressFormat.PNG, 100, _outputStream);
            else
                _bitmap.compress(Bitmap.CompressFormat.JPEG, _quality, _outputStream);

            osSize = _outputStream.size();
//        } catch (IOException e) { e.printStackTrace(); }

        double sizeD = (double) osSize;
        sizeD /= 1000000;
        mTextViewSize.setText("Rozmiar strumienia: " + sizeD);
        mTextViewSize.append("\nBitmapO size: " + _bitmap.getAllocationByteCount());

        return _outputStream;
    }

    private void compressAutomatically()
    {
        int MB100 = 100;
        int MB75 = 75;
        int MB50 = 50;
        int MB15 = 15;
        int MB02 = 2;

        double bitmapSize = 0;
        Bitmap bitmap = null;

        try{

            bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), mImageUri);
            bitmapSize = getBitmapSizeMB(bitmap);

        } catch (IOException e) { e.printStackTrace(); mTextViewSize.setText(e.getMessage()); return; }


        ByteArrayOutputStream _os = compress(bitmap, -1);

//        if(_os.size() >= MB100) { _os = compress(0); mTextViewSize.append("\n>= MB100"); }
//        else if(_os.size() >= MB75 && _os.size() < MB100) { _os = compress(25); mTextViewSize.append("\n>= MB75"); }
//        else if(_os.size() >= MB50 && _os.size() < MB75) { _os = compress(50); mTextViewSize.append("\n>= MB50"); }
//        else if(_os.size() >= MB15 && _os.size() < MB50) { _os = compress(75); mTextViewSize.append("\n>= MB15"); }
//        else if(_os.size() >= MB02 && _os.size() < MB15) { _os = compress(100); mTextViewSize.append("\n>= MB02"); }
//        else { mTextViewSize.append("\n< MB02"); }

        if(bitmapSize >= MB100) { _os = compress(bitmap, 0); mTextViewSize.append("\n>= MB100"); }
        else if(bitmapSize >= MB75 && bitmapSize < MB100) { _os = compress(bitmap,25); mTextViewSize.append("\n>= MB75"); }
        else if(bitmapSize >= MB50 && bitmapSize < MB75) { _os = compress(bitmap,50); mTextViewSize.append("\n>= MB50"); }
        else if(bitmapSize >= MB15 && bitmapSize < MB50) { _os = compress(bitmap,75); mTextViewSize.append("\n>= MB15"); }
        else if(bitmapSize >= MB02 && bitmapSize < MB15) { _os = compress(bitmap,100); mTextViewSize.append("\n>= MB02"); }
        else { mTextViewSize.append("\n< MB02"); }

//        if(_os.size() > MB15 && _os.size() < MB50)
//        {
//            for (int i = 0; i < 5; i++) {
//                _os = compress(q - i * 25);
//                if (_os.size() < MB50)
//                    break;
//            }
//        }
//        else if(_os.size() >= MB50 )
//        {
//            _os = compress(0);
//        }

        bitmapS = Base64.encodeToString(_os.toByteArray(), Base64.DEFAULT);
    }

//    public String convert(Bitmap bitmap)
//    {
//        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
//        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
//        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
//    }

    public void onRadioButtonClicked(View view){
        int radioId = radioGroup.getCheckedRadioButtonId();
        radioButton = findViewById(radioId);

        switch(radioId) {
            case R.id.radio_compress_without:
                quality = -1;
                break;
            case R.id.radio_compress_100:
                quality = 100;
                break;
            case R.id.radio_compress_75:
                quality = 75;
                break;
            case R.id.radio_compress_50:
                quality = 50;
                break;
            case R.id.radio_compress_25:
                quality = 25;
                break;
            case R.id.radio_compress_0:
                quality = 0;
                break;
        }

        Toast.makeText(this, "Wybrana " + radioButton.getText(), Toast.LENGTH_SHORT).show();
    }


    //***********************************************************************************************************************8

    public void uploadImage(int descrType, int id, String data) {
        Upload upload = new Upload(descrType, id, data, "144000.jpg");
        Call<List<Upload>> call = jsonMpegApi.uploadImage(upload);

        uploadStartTime = System.currentTimeMillis();
        call.enqueue(new Callback<List<Upload>>() {
            @Override
            public void onResponse(Call<List<Upload>> call, Response<List<Upload>> response) {
                if (!response.isSuccessful()) {
                    mTextViewMain.setText("Code: " + response.code()
                            + "Response body: " + response.body()
                            + "Response message" + response.message());

                    return;
                }
                List<Upload> uploadResponse = response.body();
                //textViewResult.setText("Wyniki wyszukiwania:");
                display(uploadResponse);

//                mTextViewMain.setText("");
//                //List<Upload> uploads = response.body();
////                mTextViewMain.setText("Wyniki wyszukiwania:");
////                display(searchResponse);
//                for(Upload upload : uploadResponse){
//                    String content = "";
//                    content += "ID: " + upload.getId() + "\n";
//                    content += "distance: " + upload.getDist()+ "\n";
//                    content += "url: " + upload.getUrl()+ "\n\n";
//
//                    mTextViewMain.append(content);
//                }
                mButtonUpload.setVisibility(View.VISIBLE);
                displayTimeMeasurements();
            }

            @Override
            public void onFailure(Call<List<Upload>> call, Throwable t) {
                mTextViewMain.setText(t.getMessage());
                mButtonUpload.setVisibility(View.VISIBLE);
                displayTimeMeasurements();
            }
        });
    }


    @SuppressLint("WrongConstant")
    private void display(List<Upload> uploadResponse)
    {
        Log.d(TAG, "display: started");

//        picRecView = (RecyclerView) findViewById(R.id.picRecView_u);

//        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
//        layoutManager.setOrientation(LinearLayout.HORIZONTAL);
//        picRecView.setLayoutManager(layoutManager);
//        //SearchRecViewAdapter adapter = new SearchRecViewAdapter(this, mTextViewUri);
//        picRecView.setAdapter(adapter);

        ArrayList<Search> searches = new ArrayList<>();

        for (int i = 0; i < uploadResponse.size(); i++)
        {
            String imageUrl = uploadResponse.get(i).getUrl();
            int id = uploadResponse.get(i).getId();
            double distance = uploadResponse.get(i).getDist();
            searches.add(new Search(id, imageUrl, distance));
        }
        adapter.setPictures(searches);
        //adapter.notifyDataSetChanged();
    }


    //***********************************************************************************************************************8

    public void initSpinner() {

        String[] descTypes = new String[]{"Dominant Color", "Scalable Color", "Color Layout", "Color Structure", "CT Browsing", "Deskryptor"};
        ArrayAdapter<String> descsAdapter = new ArrayAdapter<String>(this, R.layout.style_spinner_black, descTypes) {
            @Override
            public int getCount() {
                return descTypes.length-1;
            }
        };

        descsAdapter.setDropDownViewResource(R.layout.color_spinner_layout);
        spinner.setAdapter(descsAdapter);
        spinner.setSelection(descTypes.length-1, false);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int yourDescType = (spinner.getSelectedItemPosition() + 1);

                closeKeyboard();
                mTextViewUri.requestFocusFromTouch();
                mTextViewUri.requestFocus();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    private void init()
    {
        initSpinner();

        mTextViewMain.setVisibility(View.INVISIBLE);
        mTextViewUri.setVisibility(View.INVISIBLE);
        mButtonUpload.setVisibility(View.INVISIBLE);

        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPhotoSize();
            }
        });

        photoSize = new Double(0);

        scrollView = findViewById(R.id.scroll_view);
    }

    private void closeKeyboard()
    {
        View view = this.getCurrentFocus();
        if(view != null)
        {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void setPhotoSize(Bitmap _bitmap)
    {
        Double bitmapSizeD = getBitmapSizeMB(_bitmap);

        if(bitmapSizeD > 15)
        {
            photoSize = bitmapSizeD;
            Toast.makeText(this, "Obrazek jest zbyt duży!", Toast.LENGTH_SHORT).show();
        }
        else
        {
            photoSize = bitmapSizeD;
            Toast.makeText(this, "Wczytano obrazek", Toast.LENGTH_SHORT).show();
        }
    }

    private Double getBitmapSizeMB(Bitmap _bitmap)
    {
        int bitmapSize = _bitmap.getByteCount();
        Double bitmapSizeD = new Double(bitmapSize);
        bitmapSizeD = bitmapSizeD / 1000000;
        return bitmapSizeD;
    }

    private void showPhotoSize()
    {
//            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), mImageUri);
//            int bitmapSize = bitmap.getByteCount();
//            Double bitmapSizeD = new Double(bitmapSize);
//            bitmapSizeD = bitmapSizeD / 1000000;
//            //String formattedString = String.format("%.04f", bitmapSizeD);
//
//            Toast.makeText(this, "Wielkość obrazka: " + bitmapSizeD + " MB", Toast.LENGTH_SHORT).show();
//            photoSize = bitmapSizeD;

            Toast.makeText(this, "Wielkość obrazka: " + photoSize + " MB", Toast.LENGTH_SHORT).show();


    }



}
